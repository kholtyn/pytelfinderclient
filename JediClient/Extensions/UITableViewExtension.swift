//
//  UITableViewExtension.swift
//  JediClient
//
//  Created by Krzysztof Holtyn-EXT on 23/02/2018.
//  Copyright © 2018 Krzysztof Holtyn-EXT. All rights reserved.
//

import UIKit

extension UITableView {
    func registerCells(_ cellsClasses: [AnyClass], withBundle bundle: Bundle? = nil) {
        cellsClasses.forEach { cellClass in
            self.register(UINib(nibName: String(describing: cellClass.self), bundle: bundle), forCellReuseIdentifier: String(describing: cellClass))            
        }
    }
}
