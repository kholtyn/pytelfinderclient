//
//  JediCell.swift
//  JediClient
//
//  Created by Krzysztof Holtyn-EXT on 22/02/2018.
//  Copyright © 2018 Krzysztof Holtyn-EXT. All rights reserved.
//

import UIKit

final class JediCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var profileImageView: UIImageView!
    
    func decorate(withPerson person: Person, andStatus status: StatusType) {
        nameLabel.text = person.0
        statusLabel.text = status.statusText
        statusLabel.textColor = status.statusColorLabel
        profileImageView.image = person.1
        
        backgroundColor = status.backgroundColorCell
    }
    
    class var defaultHeight: CGFloat {
        return 64.0
    }
}
