//
//  JediViewController.swift
//  JediClient
//
//  Created by Krzysztof Holtyn-EXT on 22/02/2018.
//  Copyright © 2018 Krzysztof Holtyn-EXT. All rights reserved.
//

import UIKit
import Alamofire

class JediViewController: UIViewController {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var profileImageView: UIImageView!
    
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    var person: Person?
    var status: StatusType?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setViews()
        request()
    }
    
    func decorate(withPerson person: Person, andStatus status: StatusType) {
        self.person = person
        self.status = status
    }

    func sendRequest(user: String, enteredArea: Bool, location: String) {
        
        let urlStr = "http://10.70.53.141:5000/user-\(enteredArea ? "entered" : "leaved")-location/\(user)?location=\(location)"
        guard let url = URL(string: urlStr) else {
            print ("bad url: \(urlStr)")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            print("Entered the completionHandler")
            }.resume()
    }
    
    private func request() {
        Alamofire.request("http://10.70.53.141:5000/user-current-location/Krzysztof-Pytel").responseJSON { [weak self] response in
            
            if let json = response.result.value as? [[String: String]] {
                let locationName = json.last?["locationName"]
                let enteredDate = json.last?["enteredDate"]
                
                guard let location = locationName, let date = enteredDate else { return }
                
                self?.setData(location: location, andDate: date)
            }
        }
    }
    
    private func setViews() {
        guard let person = person, let status = status else { return }
        
        nameLabel.text = person.0
        statusLabel.text = status.statusText
        statusLabel.textColor = status.statusColorLabel
        profileImageView.image = person.1
        
        view.backgroundColor = status.backgroundColorCell
    }
    
    private func setData(location: String, andDate date: String) {
        locationLabel.text = location
        locationLabel.textColor = .black
        
        dateLabel.text = date
        dateLabel.textColor = .black
    }
}

