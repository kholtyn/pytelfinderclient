//
//  JediesListViewController.swift
//  JediClient
//
//  Created by Krzysztof Holtyn-EXT on 22/02/2018.
//  Copyright © 2018 Krzysztof Holtyn-EXT. All rights reserved.
//

import UIKit

typealias Person = (String, UIImage)

final class JediesListViewController: UIViewController {
    
    var persons: [Person] = [("Krzysztof Pytel", UIImage(named: "Krzysiek")!),
                             ("Claire Jarosz", UIImage(named: "Claire")!),
                             ("Dariusz Kuc", UIImage(named: "Dariusz")!)]
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            
            tableView.registerCells([JediCell.self])

        }
    }
}

extension JediesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return JediCell.defaultHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let jediViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JediViewController") as? JediViewController {
            jediViewController.decorate(withPerson: persons[indexPath.row], andStatus: .available)
            if let navigator = navigationController {
                navigator.pushViewController(jediViewController, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row != 0 ? false : true
    }}

extension JediesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let jediCell = tableView.dequeueReusableCell(withIdentifier: "JediCell", for: indexPath) as? JediCell else { return UITableViewCell() }
        var status: StatusType = (indexPath.row != 0 ? .unavailable : .available)
        jediCell.decorate(withPerson: persons[indexPath.row], andStatus: status)
        return jediCell
    }
}
