//
//  StatusEnum.swift
//  JediClient
//
//  Created by Krzysztof Holtyn-EXT on 22/02/2018.
//  Copyright © 2018 Krzysztof Holtyn-EXT. All rights reserved.
//

import UIKit

enum StatusType {
    case available
    case unavailable
    
    var backgroundColorCell: UIColor {
        switch self {
        case .available:
            return UIColor(red: 230.0/255.0, green: 242.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        case .unavailable:
            return UIColor(red: 242.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        }
    }
    
    var statusColorLabel: UIColor {
        switch self {
        case .available:
            return UIColor(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        case .unavailable:
            return UIColor(red: 128.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        }
    }
    
    var statusText: String {
        switch self {
        case .available:
            return "Dostępny"
        case .unavailable:
            return "Zajęty"
        }
    }
}
